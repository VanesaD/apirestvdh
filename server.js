var express = require('express');
var app = express();
//para parsear el body del json
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMLabURL = "https://api.mlab.com/api/1/databases/techu_vdh/collections/";
var mLabAPIKey = "apiKey=eEVugPN9S1a7TP8AkPj9iCpUz_815xPy";
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto " + port);

//creamos metodo get
// req y res los rellena el framework para enviar al cliente
//ruta de bienvenida '/apitechu/v1'
app.get ('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send(
      {
      "msg":"Hola desde ApitechU molona"
      }
    )
  }
);


//ruta de usuarios '/apitecu/v1/user't
app.get('/apitechu/v1/users',
  function(req,res){
    console.log("GET /apitechu/v1/users");
    //root -> indica la ruta. __dirname tiene el valor del directorio donde se ejecuta el script
   res.sendFile('usuarios.json',{root: __dirname});
  }
);

//version2 '/apitecu/v2/user'
app.get('/apitechu/v2/users',
  function(req,res){  //funcion manejadora del GET
    console.log("GET /apitechu/v2/users");
    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");
    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body){
        var response = !err ? body : {
        "msg":"Error obteniendo usuarios."
        }
        res.send(response);
      }
    );
  }
);

//version2 con id en entrada
app.get('/apitechu/v2/users/:id',
  function(req,res){
    console.log("GET /apitechu/v2/users/:id");//https://api.mlab.com/api/1/databases/techu_vdh/collections/user?q={
    var id = req.params.id;
    var query = 'q={"id": ' + id + '}';

    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){

        if (err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
        }
        else {
          if (body.length > 0) {
            response = body[0];
          }
          else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
      res.send(response);

        //var response = !err ? body : {
        //"msg":"Error obteniendo usuarios."
        //}
        //res.send(response);
      }
    );
  }
);

/*
app.post('/apitechu/v1/users',
  function(req,res){
    console.log("POST /apitechu/v1/users");
    //console.log(req);

    console.log("first_name es: " + req.headers.first_name);
    console.log("last_name es: " + req.headers.last_name);
    console.log("country es: " + req.headers.country);

    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "country" : req.headers.country
    };

    var users = require('./usuarios.json');
    users.push(newUser);

    writeUserDataToFile(users);

    //console.log("Usuario añadido correctamente");

    //res.send(users);

    res.send(
      {
      "msg" : "Usuario añadido con exito"
      }
    );

  }
);
*/
app.post('/apitechu/v1/users',
  function(req,res){
    console.log("POST /apitechu/v1/users");
    //console.log(req);

    console.log("first_name es: " + req.body.first_name);
    console.log("last_name es: " + req.body.last_name);
    console.log("country es: " + req.body.country);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    var users = require('./usuarios.json');
    users.push(newUser);

    writeUserDataToFile(users);

    //console.log("Usuario añadido correctamente");

    //res.send(users);

    res.send(
      {
      "msg" : "Usuario añadido con exito"
      }
    );

  }
);

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
    console.log("Parámetro");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

    res.send(
      {
      "msg" : "Si lo sé no vengo"
      }
    );

  }
);

function writeUserDataToFile(data){
  var fs=require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usulog.json",
    jsonUserData,
    "utf8",
    function(err){
      if (err){
        console.log(err);
      }  else{
        console.log("Usuario persistido");
      }

    }
  );
}

app.delete('/apitechu/v1/users/:id',
  function(req,res){
    console.log("DEL /apitechu/v1/users/:id");

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);

    writeUserDataToFile(users);

    res.send(
      {
      "msg" : "Usuario borrado con exito"
      }
    );

  }
);

//POST del LOGGIN
app.post('/apitechu/v1/login',
  function(req,res){
    console.log("POST /apitechu/v1/login");
    //console.log(req);

    //console.log("email es: " + req.body.email);
    //console.log("password es: " + req.body.password);

    var newLog = {
      "email" : req.body.email,
      "password" : req.body.password
    };

    console.log("email es: " + req.body.email);
    console.log("password es: " + req.body.password);


    var users = require('./usulog.json');

    for (user of users){

      if (user.email == newLog.email && user.password == newLog.password){
        //console.log("email correcto");
            user.logged=true;
            break;
      }

    }

    if(user.logged){
      console.log(user);
      //hay que escribir users para escribir todo el fichero; si escribes
      // user escribe solo ese usuario
      writeUserDataToFile(users);
      console.log("log correcto");
      var salida = {
        "msg" : "login correcto",
        "id" : user.id
      };

    }
    else{
      console.log("log incorrecto");

      var salida = {
        "msg" : "login incorrecto",
      };

    }
    res.send(salida);
  }
);

//POST del LOGOUT
app.post('/apitechu/v1/logout',
  function(req,res){
    console.log("POST /apitechu/v1/logout");

    var newLog = {
      "id" : req.body.id,
    };

    console.log("id es: " + newLog.id);

    var users = require('./usulog.json');


    for (user of users){

      if (user.id == newLog.id){

            if (user.logged){
              console.log("usuario logado");
              delete user.logged;
              writeUserDataToFile(users);
              var salida = {
                "msg" : "logout correcto",
              };
              break;
            }
            else{
              console.log("usuario no logado");
              var salida = {
                "msg" : "logout incorrecto: usuario no logado"
              };
              break;
            }
      }
      else{
        console.log("usuario no encontrado");
        var salida = {
          "msg" : "logout incorrecto: usuario no encontrado"
        };
      }

    }

    res.send(salida);

  }
);

//POST del LOGGIN con acceso a MongoDB
app.post('/apitechu/v2/login',
  function(req,res){
    console.log("POST /apitechu/v2/login");

    console.log("email es: " + req.body.email);
    console.log("password es: " + req.body.password);

    var query = 'q={"email": "' + req.body.email + '","password": "'+ req.body.password +'"}';
    console.log("query: " + query);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion Cliente creada");

    var response = {"msg" : "mensaje salida"};

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
        var encontrado = false;
        if (err) {
          response.msg = "Error obteniendo usuario.";
          res.status(500);
          //console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
          res.send(response);
        }
        else {
          if (body.length > 0) {
            response.msg = "Usuario encontrado.";
            response.user = body[0];
            encontrado=true;
          }
          else {
            response.msg = "Usuario no encontrado.";
            res.status(404)
            //console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
            res.send(response);
          }
        }

        console.log(encontrado);
        console.log("Valor response : " + JSON.stringify(response));

        if (encontrado){
          console.log("Usuario encontrado. Actualizamos logged:true");
          var query2 = 'q={"id": ' + body[0].id + '}';
          var putBody = '{"$set":{"logged":true}}';
          console.log("query2: " + query2);
          console.log("putBody: " + putBody);

          httpClient.put("user?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPut, resMLabPut, bodyPut){
            console.log("user?" + query2 + "&" + mLabAPIKey );
            if (errPut) {
                console.log("error en logon");
                response.msg = "Error en logon.";
                res.status(500);
               }
            else {
              console.log("logon OK");
              response.msg = "logon ok";
            }
            //console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
            res.send(response);
          }
          );

        }
        //console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
        //res.send(response);
       }
     );

  }
);

//POST del LOGOUT con acceso a MongoDB
app.post('/apitechu/v2/:id/logout',
  function(req,res){
    console.log("POST /apitechu/v2/:id/logout");

    /*var user_id = req.params.id;
    var query = 'q={"user_id": ' + user_id + '}';

    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion creada");

    httpClient.get("account?" + query + "&" + mLabAPIKey,
    */



    console.log("id es: " + req.params.id);

    var query = 'q={"id": ' + req.params.id + '}';
    console.log("query: " + query);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion Cliente creada");

    var response = {"msg" : "mensaje salida"};

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
        var encontrado = false;
        if (err) {
          response.msg = "Error obteniendo usuario.";
          res.status(500);
          //console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
          res.send(response);
        }
        else {
          console.log(body[0]);
          if (body.length > 0) {
            response.msg = "Usuario encontrado.";
            response.user = body[0];
            encontrado=true;
          }
          else {
            response.msg = "Usuario no encontrado.";
            res.status(404);
            //console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
            res.send(response);
          }
        }

        console.log(encontrado);
        console.log("Valor response : " + JSON.stringify(response));

        if (encontrado){
          if (body[0].logged) { //Comprobamos si el usuario estaba logado
            console.log("Usuario encontrado. Hacemos logout");
            var query2 = 'q={"id": ' + body[0].id + '}';
            var putBody = '{"$unset":{"logged":""}}';
            console.log("query2: " + query2);
            console.log("putBody: " + putBody);

            httpClient.put("user?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPut, resMLabPut, bodyPut){
              console.log("user?" + query2 + "&" + mLabAPIKey );
              if (errPut) {
                console.log("error en logout");
                response.msg = "Error en logout";
                res.status(500);
              }
              else {
                console.log("logout OK");
                response.msg = "logout ok";
              }
              //console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
              res.send(response);
            }
            );
          }
        }

       }
     );

  }
);


//version2 de cuentas con id en entrada
//https://api.mlab.com/api/1/databases/techu_vdh/collections/account?q={"user_id":22}&apiKey=eEVugPN9S1a7TP8AkPj9iCpUz_815xPy
app.get('/apitechu/v2/users/:id/accounts',
  function(req,res){
    console.log("GET /apitechu/v2/users/:id/accounts");
    var user_id = req.params.id;
    var query = 'q={"user_id": ' + user_id + '}';

    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion creada");

    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){

        if (err) {
          response = {"msg" : "Error obteniendo cuentas de usuario."}
          res.status(500);
        }
        else {
          if (body.length > 0) {
            response = body;
          }
          else {
            response = {"msg" : "Cuentas de usuario no encontradas."}
            res.status(404);
          }
        }
      res.send(response);

        //var response = !err ? body : {
        //"msg":"Error obteniendo usuarios."
        //}
        //res.send(response);
      }
    );
  }
);

//version2 de movimientos de una cuenta con id_cta en entrada
//https://api.mlab.com/api/1/databases/techu_vdh/collections/movement?q={"account_id":47}&apiKey=eEVugPN9S1a7TP8AkPj9iCpUz_815xPy
app.get('/apitechu/v2/account/:id/movements',
  function(req,res){
    console.log("GET /apitechu/v2/account/:id/movements");
    var account_id = req.params.id;
    var query = 'q={"account_id": ' + account_id + '}' + '&s={"date":1}';

    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion creada");

    httpClient.get("movement?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){

        if (err) {
          response = {"msg" : "Error obteniendo movimientos de cuenta."}
          res.status(500);
        }
        else {
          if (body.length > 0) {
            response = body;
          }
          else {
            response = {"msg" : "Movimientos de cuenta no encontrados."}
            res.status(404);
          }
        }
      res.send(response);

      }
    );
  }
);

//Alta de cliente
app.post('/apitechu/v2/user',
  function(req,res){

    console.log("POST /v2/user");
    console.log("Datos de entrada del nuevo usuario:");
    console.log("- first_name : " + req.body.first_name);
    console.log("- last_name : " + req.body.last_name);
    console.log("- email :" + req.body.email);
    console.log("- password : " + req.body.password);

    var httpClient = requestJson.createClient(baseMLabURL);

    //Comprobamos si existe algún usuario con el mismo email.
    var query = 'q={"email":"' + req.body.email + '"}';
    console.log("Query consulta -> " + query);
    console.log("Cliente creado");
    var response = {"msg" : "mensaje salida"};

    httpClient.get("user?" + query + "&"+   mLabAPIKey,

      function(err, resMLab, body){
      var encontrado = true;
      if (err) {
        response.msg = "Error obteniendo usuario.";
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          response.msg = "GET - USUARIO EXISTENTE";
          res.status(404);
          res.send(response);
        } else {
          encontrado = false;
          response.msg = "GET - USUARIO NO ENCONTRADO";
        }
      }
      console.log("Valor var reponse : " + JSON.stringify(response));
      if(!encontrado){
        console.log("Buscamos el último usuario dado de alta");
        query = 's={"id":-1}&sk=0&l=1';
        httpClient.get("user?" + query + "&"+   mLabAPIKey,
        function(err, resMLab, body){
          var maxuser = false;
          if (err) {
            response.msg = "Error obteniendo máximo id.";
            res.status(500);
            res.send(response);
          } else {
            if (body.length > 0) {
              response = body[0];
              maxuser = true;
            } else {
              response.msg = "GET - ULTIMO USUARIO NO ENCONTRADO";
              res.status(404);
              res.send(response);
            }
          }
          if(maxuser){
              //Damos de alta el usuario
              var newid = response.id + 1;
              var postBody = '{"id":'+ newid +',"first_name":"'+ req.body.first_name +'","last_name": "'+ req.body.last_name+'","email": "'+req.body.email +'","password": "'+ req.body.password +'"}';
              console.log("postBody - " + postBody);
              httpClient.post("user?"+ mLabAPIKey,JSON.parse(postBody),
              function(errPOST, resMLabPOST, bodyPOST) {
                if (errPOST) {
                  response.msg = "Error al hacer ALTA.";
                  res.status(500);
                } else {
                  //response = {"msg" : "PUT - Login correcto."};
                  response = postBody;
                }
                res.send(response);
              });
            }
        });
      }
      console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
      }
    );
});

/*
* 29-04-2018
* Alta de una cuenta
*/

app.post('/apitechu/v2/user/account',
  function(req,res){

    console.log("POST /v2/users/account");
    console.log("Datos de entrada del nuevo usuario:");
    console.log("- id : " + req.body.id);

    var httpClient = requestJson.createClient(baseMLabURL);
    var query = 's={"account_id":-1}&sk=0&l=1';

    httpClient.get("account?" + query + "&"+   mLabAPIKey,
      function(err, resMLab, body){
        var maxaccount = false;
        if (err) {
            response.msg = "Error obteniendo máximo id.";
            res.status(500);
            res.send(response);
        } else {
            if (body.length > 0) {
              response = body[0];
              maxaccount = true;
            } else {
              response.msg = "GET - MÁXIMO ID CUENTA NO ENCONTRADO";
              res.status(404);
              res.send(response);
            }
        }
        if(maxaccount){
          console.log("Hemos recuperado el maximo id de las cuentas");
          console.log("MaxId : " + response.account_id);
          //Damos de alta la cuenta
          var newid = response.account_id + 1;
          var numAle = getRandomInt(01,99);
          var alfaAle1 = getRandom(4);
          var alfaAle2 = getRandom(4);
          var alfaAle3 = getRandom(4);
          var alfaAle4 = getRandom(4);;
          var iban = "ES" + numAle + " 0182 " + alfaAle1 + " "+ alfaAle2 + " " + alfaAle3 + " " + alfaAle4;
          console.log("Código iban -->" + iban);
          var postBody = '{"account_id":'+ newid +',"user_id":'+ req.body.id +',"IBAN": "'+iban+'","balance": 50.01}';
          console.log("postBody - " +postBody);
          var errorAlta = false;
          httpClient.post("account?"+   mLabAPIKey,JSON.parse(postBody),
            function(errPOST, resMLabPOST, bodyPOST) {
              if (errPOST) {
                response.msg = "Error al hacer ALTA.";
                res.status(500);
                errorAlta = true;
              } else {
                //response = {"msg" : "PUT - Login correcto."};
                response = postBody;
                res.send(response);
                //errorAlta = true;
              }
              if(errorAlta){
                console.log("Error en el alta de la cuenta");
                var deleteBody = '[]';
                var query = 'q={"id":' + req.body.id + '}';
                console.log("user?"+ query+"&"+mLabAPIKey);
                console.log("body : " +deleteBody);
                //es un put pero con el body en vacio []
                httpClient.put("user?"+ query+"&"+mLabAPIKey,JSON.parse(deleteBody),
                  function(err, resMLab, body){
                    if (err) {
                      response = {"msg" : "Error borrado usuario."};
                      res.status(500);
                    } else {
                      console.log("Borrado usuario --> " + req.body.id);
                      response = {"msg" : "Error alta cuenta. Usuario borrado"};
                    }
                    res.send(response);
                  });
              }
          });
        }
    });
});

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandom(longitud)
{
  var caracteres = "ABCDEFGHIJKLMNPQRTUVWXYZ123456789";
  var salida = "";
  for (i=0; i<longitud; i++) salida += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
  return salida;
}


//Alta de movimientos
app.post('/apitechu/v2/altamov',
  function(req,res){
    console.log("POST /apitechu/v2/altamov");

    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();

    if(dd<10) {
      dd='0'+dd
    }

    if(mm<10) {
      mm='0'+mm
    }

    hoy = yyyy+'/'+mm+'/'+dd;

    console.log("Datos de entrada del nuevo movimiento:");
    //faltaria id_cuenta
    console.log("- account_id: " + req.body.account_id);
    console.log("- description : " + req.body.description);
    console.log("- amount :" + req.body.amount);
    console.log("- date : " + hoy);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion Cliente creada");

    var response = {"msg" : "mensaje salida"};

    query = 's={"movement_id":-1}&sk=0&l=1';
    console.log(baseMLabURL + "movement?" + query + "&");

    httpClient.get("movement?" + query + "&"+   mLabAPIKey,
      function(err, resMLab, body){
        var maxmov = false;
        var alta = false;
        console.log("Buscamos el último movimiento de la cta de entrada");
        if (err) {
          response.msg = "Error obteniendo máximo movement_id.";
          console.log("500-Error obteniendo máximo movement_id.");
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0) {
            maxmov = true;
            response.movement= body[0];
            console.log(body[0]);
          } else {
            response.msg = "Maximo movement_id no encontrado";
            console.log("404-Maximo movement_id no encontrado");
            res.status(404);
            res.send(response);
          }
        }
        console.log("maxmov: "+ maxmov);
        if(maxmov){
          //Damos de alta el movimiento
          console.log(response.movement.movement_id);
          var newmov = response.movement.movement_id + 1;
          console.log("newmov: "+ newmov);
          //var postBody = 'q={"movement_id":'+ newmov +',"account_id":"'+ req.body.account_id +'","description": "'+ req.body.description+'","amount": "'+req.body.amount +'","date": "'+ hoy +'"}';
          var postBody = '{"movement_id":'+ newmov +',"account_id":'+ req.body.account_id +',"description": "'+ req.body.description+'","amount": '+req.body.amount +',"date": "'+ hoy +'"}';
          console.log("postBody - " +postBody);

          httpClient.post("movement?"+  mLabAPIKey,JSON.parse(postBody),
            function(errPOST, resMLabPOST, bodyPOST) {
              if (errPOST) {
                console.log("Error en alta movimiento");
                response.msg = "Error al hacer Alta de Movimientos.";
                res.status(500);
                res.send(response);
              } else {
                alta = true;
                console.log("Alta: " + alta);
                console.log("Alta movimiento correcta");
                response.movement = bodyPOST;
              }
              //res.send(response);

              //Actualizar saldo en cuenta
              console.log("alta: " + alta);
              if (alta){
                var query2 = 'q={"account_id": ' + req.body.account_id +'}';
                console.log("query2: " + query2);

                httpClient.get("account?" + query2 + "&" + mLabAPIKey,
                  function(err2, resMLab2, body2){
                    var encontrado = false;
                    if (err2) {
                      response.msg = "Error obteniendo cuenta.";
                      console.log("Error obteniendo cuenta.");
                      res.status(500);
                      res.send(response);
                    }
                    else {
                      if (body.length > 0) {
                        response.msg = "Cuenta encontrada.";
                        console.log("Cuenta encontrada.");
                        response.account = body2[0];
                        encontrado=true;
                      }
                      else {
                        response.msg = "Cuenta no encontrada.";
                        console.log("Cuenta no encontrada.");
                        res.status(404);
                        res.send(response);
                      }
                    }

                    console.log(encontrado);
                    console.log("Valor response : " + JSON.stringify(response));

                    if (encontrado){
                      console.log("Cuenta encontrada. Actualizamos balance");
                      var query3 = 'q={"account_id": ' + body2[0].account_id + '}';
                      console.log("query3: " + query3);

                      console.log("balance: " + body2[0].balance);
                      var newbalance = (body2[0].balance + response.movement.amount).toFixed(2);
                      console.log("newbalance: " + newbalance);

                      var putBody = '{"$set":{"balance":' + newbalance + '}}';
                      console.log("putBody: " + putBody);

                      httpClient.put("account?" + query3 + "&" + mLabAPIKey, JSON.parse(putBody),
                        function(errPut, resMLabPut, bodyPut){
                          console.log("account?" + query3 + "&" + mLabAPIKey );
                          if (errPut) {
                            console.log("error en actualizacion balance");
                            response.msg = "Error en actualizacion balance.";
                            res.status(500);
                          }
                          else {
                            console.log("Actualizacion balance OK");
                            response.msg = "Actualizacion balance ok";
                            response.account.balance = parseFloat(newbalance);
                          }
                          res.send(response);
                        }
                      );//cierra "httpClient.put("account?" + query3"
                    }//cierra "if(encontrado)"
                  }
                );// cierra "httpClient.get("account?" + query2"
              }// cierra "if (alta)"
            }
          );//cierra "httpClient.post("movement?""
        }//cierra "if(maxmov)"
      }
    );//cierra "httpClient.get("movement?" + query + "&"+   mLabAPIKey,""
  }
);
