# Imagen raiz
FROM node

# Carpeta raiz
WORKDIR /apitechu

# Copia de archivos
ADD . /apitechu

# Exponer puerto (del contenedor)
EXPOSE 3000

# Instalar dependencias
#RUN npm install

# Añadir volume
VOLUME ['/logs']

# Comando de inicializacion
CMD ["npm", "start"]
